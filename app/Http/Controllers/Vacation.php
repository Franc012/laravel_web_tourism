<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Vacation extends Controller
{
    public function index(){
        return view('home');
    }
    public function packages(){
        return view('pacotes');
    }
    public function destinies(){
        return view('destinos');
    }
    public function cruises(){
        return view('cruzeiros');
    }
    public function about(){
        return view('sobre');
    }
}
