@extends('layouts.main')

@section('title', 'Sobre Nós')

@section('content')
    <div class="about-container">
        <img src="/img/about.jpg" alt="Sobre Nós" class="about-background">
        <div class="about-content">
            <h1>Sobre Nós</h1>
            <p>Bem-vindo à [Nome da Empresa]! Somos dedicados a oferecer as melhores experiências de viagem. Nossa missão é proporcionar momentos inesquecíveis e destinos de tirar o fôlego. Com uma equipe experiente e apaixonada, estamos aqui para tornar seus sonhos de viagem realidade.</p>
        </div>
    </div>
@endsection

    <link href="{{ asset('css/sobre.css') }}" rel="stylesheet">

