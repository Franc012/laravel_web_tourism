@extends('layouts.main')

@section('title', 'Main Page')

@section('content')

    <div class="banner-container">
        <img src="/img/bannermain.jpg" alt="Banner" class="banner-image">
        <div class="banner-text">
            <h1>Embarque nessa viagem</h1>
            <div class="main_text">
                <p>
                Oferecemos pacotes de viagens incríveis que vão transformar seus sonhos em realidade. 
                Seja explorando praias paradisíacas, desvendando culturas exóticas ou relaxando em cruzeiros luxuosos, temos o pacote perfeito para você.
                </p>
            </div>
        </div>
    </div>

@endsection
