<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

use App\Http\Controllers\Vacation;
use App\Http\Controllers\Users;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/Home', [Vacation::class, 'index']);
Route::get('/Pacotes', [Vacation::class, 'packages']);
Route::get('/Destinos', [Vacation::class, 'destinies']);
Route::get('/Cruzeiros', [Vacation::class, 'cruises']);
Route::get('/Sobre', [Vacation::class, 'about']);